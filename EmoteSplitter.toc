## Interface: 70300
## Title: Emote Splitter
## Version: 1.3.3
## Notes: Unlocks chatbox and splits long emotes.
## Author: Tammya-MoonGuard
## OptionalDeps: UnlimitedChatMessage
## SavedVariables: EmoteSplitterSaved
   
embeds.xml
EmoteSplitter.lua
emoteprotection.lua
options.lua